import { View, ScrollView, Image, Text } from '@tarojs/components'
import Taro, { useLoad } from '@tarojs/taro'
import './guidance.scss'
import React, {useEffect, useState} from "react";
import service from '../services/index'
import {onAppShow} from "@tarojs/plugin-platform-harmony-hybrid/dist/api";

const List = (props) => {

}
export default function Guidance() {
  const [lists, setLists] = useState([]);
  const [total, setTotal] = useState(0);
  useEffect(()=>{
    loadMore()
  }, []);
  function loadMore() {
    console.log('加载中...')
    // Taro.showLoading({
    //   title: '加载中...'
    // })
    // setTimeout(()=>{Taro.hideLoading()},5000)
    service.getNews({currentPage:1,pageSize:20,filter:{}}).then(res => {
      // Taro.hideLoading()
      if(res.success === true && res.data) {
        setLists(res.data.list)
        setTotal(res.data.total)
      }
    })
  }
  function navLink(data){
    Taro.preload({...data})
    Taro.navigateTo({url: '/pages/components/newsDetail/newsDetail'})
  }
  return (
    <ScrollView style='height:100vh' scrollY scrollWithAnimation
      onScrollToLower={loadMore}>
      <View className='container'>
        {lists.length ? lists && lists.map((item,idx) => {
          return (
            <View key={idx} className='article_item flex-r-h bd_adius8' onClick={()=> navLink(item)}>
              <View className="img_part flex-c-h just-center">
                <Image mode="widthFix" className='article_img bd_adius8 mar-r15'  src={item.thumbnailUrl}/>
              </View>
              <View className="flex-1">
                <View className="title pad-b5 text_one ft-14">{item.title}</View>
                <View className="flex-r-h just-between">
                  <Text className='ft-12'>浏览量：{item.viewTotal}</Text>
                  <Text className='ft-12'>发布时间：{item.createdTime}</Text>
                </View>
              </View>
            </View>
          )
        }):
          <View className="no_msg flex-c-h">
            <Image className='no_msg_img' src={require('../../assets/noMsg.png')} />
            <View className='flex-r-h mar-t10'>
              <Text className="ft-14">暂无数据~</Text>
            </View>
          </View>
        }
      </View>
    </ScrollView>
  )
}
