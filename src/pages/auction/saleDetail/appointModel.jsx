import {View, Picker, Text, Input, Button} from '@tarojs/components'
import Taro from '@tarojs/taro'
import React, {useState, useEffect} from "react";
import service from './../../services/index'

export default class AppointModel extends React.Component{
  constructor(props){
    super(props)
    console.log(props)
  }
  state = {
    selector: ['请选择看样时间', '今天', '明天', '后天'],
    selectorChecked: '请选择看样时间',
    name: '',
    phone: '',
  };

  onChange = e => {
    this.setState({
      selectorChecked: this.state.selector[e.detail.value]
    })
  };
  nameChange = e =>{
    this.setState({
      name: e.detail.value
    })
  };
  phoneChange = e =>{
    this.setState({
      phone: e.detail.value
    })
  };
  subAppoint = e => {
    const {selectorChecked, phone, name} = this.state;
    service.appointAuction({subjectId: this.props.id,selectorChecked, contact: phone, name}).then(res =>{
      if(res.success){
        Taro.showToast({title:'预约成功',});
        this.props.closeModel(false)
      }else{
        Taro.showToast({title:res.msg,icon: 'none'});
      }
    })
  };
  modelTap = e =>{
    if(e.target.idx === '1' || e.target.dataset.idx === '1'){
      this.props.closeModel(false)
    }
  }
  render(){
    return(
      <View className="appoint_model" onClick={this.modelTap} idx='1' data-idx='1'>
        <View className="appoint_part flex-c-h" idx='2' data-idx='2'>
          <View className="ft-20 mar-b20">请填写预约资料</View>
          <Picker className='appoint_item ' mode='selector' range={this.state.selector} onChange={this.onChange} value={this.state.selectorChecked}>
            <View className="flex-r-h just-between">
              <Text className='picker ft-14 t-333'>{this.state.selectorChecked}</Text>
              <View className="iconfont icon-bottom ft-14 pad-r5" />
            </View>
          </Picker>
          <View className="appoint_item">
            <Input value={this.state.name} onInput={this.nameChange} className='name ft-14 t-333' placeholder='请输入您的姓名' />
          </View>
          <View className="appoint_item">
            <Input value={this.state.phone} onInput={this.phoneChange} className='phone ft-14 t-333' placeholder='请输入您的手机号' />
          </View>
          <Button className="appoint_btn ft-16 mar-t10" onClick={this.subAppoint}>提交预约</Button>
        </View>
      </View>
    )
  }
}
