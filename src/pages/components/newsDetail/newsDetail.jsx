import { View, Text, Image, RichText } from '@tarojs/components'
import Taro,{ useLoad } from '@tarojs/taro'
import './newsDetail.scss'
import React, {useEffect, useState} from "react";
export default function NewsDetail() {
  const [detail, setDetail] = useState({})
  useEffect(()=>{
    console.log(Taro.getCurrentInstance().preloadData)
    setDetail(Taro.getCurrentInstance().preloadData)
  },[])
  return (
    <View className='news_detail'>
      <Image mode="widthFix" className='news_img' src={detail.thumbnailUrl} />
      <View className="news_info">
        <View className="title ft-16 t_c pad-b10 font-w600">{detail.title}</View>
        <View className="flex-r-h just-between pad-b5">
          <Text className='ft-12'>浏览量：{detail.viewTotal}</Text>
          <Text className='ft-12'>发布时间：{detail.createdTime}</Text>
        </View>
        <RichText nodes={detail.content} className="content" />
      </View>
    </View>
  )
}
