import {View, Image, Text, ScrollView} from '@tarojs/components'
import Taro, { useLoad, useRouter, getCurrentInstance } from '@tarojs/taro'
import React, {useEffect, useState} from "react";
import List from '../../../components/list/list'
import './appointList.scss'
import service from './../../../services/index'
export default function AppointList() {
  const [type, setType] = useState('1');
  const [btnText, setBtnText] = useState('去预约');
  const [lists, setLists] = useState([]);
  useEffect(()=>{
    const { router } = getCurrentInstance();
    setType(router.params.type)
    if(router.params.type === '2') {
      Taro.setNavigationBarTitle({title: '浏览记录'});
      setBtnText('去浏览')
      let historyList = Taro.getStorageSync('browsing_history') || [];
      setLists(historyList.reverse())
      return;
    }
    getList()
  }, []);
  function getList() {
    let data = {
      currentPage: 1,
      pageSize: 20,
      filter:{userId: Taro.getStorageSync('userInfo').id}
    }
    if(type === '1'){
      service.getAppoint(data).then(res => {
        if(res.success){
          setLists(res.data.list)
        }
      })
    }
    if(type === '2') {
      // service.getViewList(data).then(res => {
      //   if(res.success){
      //     setLists(res.data.list)
      //   }
      // })
    }
  }
  function goBrowse() {
    Taro.switchTab({url: '/pages/auction/auction'})
  }
  function loadMore() {
    console.log('加载更多~')
  }
  function navLink(id){
    Taro.navigateTo({url: `/pages/auction/saleDetail/saleDetail?id=${id}`})
  }
  return (
    <View className="appoint_list ">
      {lists && lists.length ? <ScrollView style='height: 100vh' scrollY scrollWithAnimation
                onScrollToLower={loadMore}>
          <View className='container bg-e8'>
            {lists.map((item,idx) => {
              return type == '2' ? <List list={item} key={idx} /> :
                <View className="appoint_list_item mar-t5">
                  <View className="ft-14 font-w600">标的物：{item.subjectName}</View>
                  <View className="ft-12 pad-t5">编号：{item.subjectCode}</View>
                  <View className="flex-r-h just-between pad-t5">
                    <View className="ft-12">预约人：{item.name}</View>
                    <View className="ft-12">联系电话：{item.contantNum}</View>
                  </View>
                  <View className="flex-r-h just-between pad-t5">
                    <View className="ft-12">预约时间：{item.createdTime}</View>
                    <View className="ft-14"/>
                  </View>

                </View>
            })}
          </View>
        </ScrollView>:
        <View className="no_msg flex-c-h">
          <Image className='no_msg_img' src={require('../../../../assets/noMsg.png')} />
          <View className='flex-r-h mar-t10'>
            <Text className="ft-14">暂无数据，</Text>
            <View className="ft-14 btn" onClick={goBrowse}>{btnText}</View>
          </View>
        </View>
      }
    </View>
  )
}
