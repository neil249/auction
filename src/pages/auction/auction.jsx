import {View, Text, Image, Input, ScrollView} from '@tarojs/components'
import Taro, { useLoad, useShareAppMessage, useShareTimeline } from '@tarojs/taro'
import './auction.scss'
import React, {useState, useEffect, useReducer } from "react";
import Pop from './pop'
import List from "../components/list/list";
import service from '../services/index'

const SearchPart = (props) =>{
  const [keyVal, setKeyVal] = useState('');
  const [loginStatus, setLoginStatus] = useState(false);
  useEffect(()=>{
    if(Taro.getStorageSync('token') && Taro.getStorageSync('loginStatus')){
      setLoginStatus(true);
    }
  })
  function keyValChange(e){
    setKeyVal(e.target.value)
    props.searchTap(e.target.value)
  }
  function clearTap(){
    setKeyVal('');
    props.searchTap('')
  }
  return(
    <View className="container search_part">
      <View className='flex-r-h just-between'>
        <View className="logo mar-r10">
          <Image className='logo_img' src={require('../../assets/logo.png')} />
        </View>
        <View className="search flex-r-h">
          <View className='iconfont icon-search t-gray' />
          <Input value={keyVal} onInput={keyValChange} className='search_ipt mar-l10 ft-14' placeholder='请输入要查询的标的物' />
          {keyVal && <View onClick={()=>clearTap()} className='iconfont icon-clear t-gray' />}
        </View>
        {!loginStatus && !keyVal && <View className="login_btn ft-16 mar-l10" onClick={()=> Taro.switchTab({url: '/pages/user/user'})}>登录</View>}
        {/*{keyVal && <View className="login_btn ft-16 mar-l10" onClick={()=> props.searchTap(keyVal)}>搜索</View>}*/}
      </View>
    </View>
  )
}
export default function Auction() {
  useEffect(() => {
    loadMore()
  }, [lists, total, currentPage]);
  useShareAppMessage(res => {
    return {
      title: '众拍服务',
      path: `/pages/home/home`
    }
  })
  useShareTimeline(() => {
    console.log('onShareTimeline')
    return {
      title: '众拍服务',
      query: `/pages/home/home`,
    }
  })
  let filterDefault = {key: null, beginTime: null, endTime: null, provinceCode: null, cityCode: null, type: null, auctionState: null, orderState:  null, orderBy: null, priceRange: null};
  const [filter, setFilter] = useState(filterDefault);
  const [lists, setLists] = useState([]);
  const [total, setTotal] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const loadMore = (val = '', filters = filter) => {
    if(val === 'more'){
      if(total < (currentPage * 20)){
        console.log(`加载到底了`)
        return;
      }else{
        setCurrentPage(currentPage + 1)
      }
    }
    let data = {
      currentPage:currentPage,
      pageSize: 20,
      filter: filters
    };
    service.getAuctions(data).then(res => {
      if(res.success === true && res.data) {
        let oldList = JSON.parse(JSON.stringify(lists))
        setLists(pre =>{
          return val === 'more'? oldList.concat([...res.data.list]) : [...res.data.list];
        });
        setTotal(pre => {return res.data.total})
      }
    })
  };
  const subTap=(data) =>{
    let filters = {
      key: '',
      beginTime: data.beginTime,
      endTime: data.endTime,
      provinceCode: data.provinceCode,
      cityCode: data.cityCode,
      type: data.type,
      auctionState: data.auctionState,
      orderState:  null,
      orderBy: null,
      courtId: data.courtId,
      priceStart: data.priceRange ? data.priceRange[0] : null,
      priceEnd: data.priceRange ? data.priceRange[1] : null,
    }
    setLists(prevData=>{return []});
    setCurrentPage(pre => 1);
    setFilter(pre =>{return filters});
    console.log(data, filters)
    loadMore('', filters);
  };
  const searchTap = (data) =>{
    let filters = data ?  {key: data } :  filter;
    console.log(data, filters)
    setLists(prevData=>{return []});
    setCurrentPage(pre => 1);
    loadMore('', filters);
  };
  return (
    <View className='auction '>
      <SearchPart searchTap={searchTap} />
      <Pop subTap={(e)=>subTap(e)} />
      <View className='sale_body pad-t15'>
        {
          lists.length ? <ScrollView style='height: 100%' scrollY scrollWithAnimation
                    onScrollToLower={()=>loadMore('more')}>
            { lists.map((item, idx) => <List list={item}  key={idx} />) }
          </ScrollView> :
            <View className="no_msg flex-c-h">
              <Image className='no_msg_img' src={require('../../assets/noMsg.png')} />
              <View className='flex-r-h mar-t10'>
                <Text className="ft-14">暂无数据~</Text>
              </View>
            </View>
        }
      </View>
    </View>
  )
}
