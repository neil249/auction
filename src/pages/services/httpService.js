import Taro from '@tarojs/taro';
import apiConfig from './apiConfig'
import {taro} from "@tarojs/plugin-platform-harmony-hybrid/dist/taroApis";

//网络请求拦截器
const interceptor = function (chain) {
  const requestParams = chain.requestParams
  const { method, data, url } = requestParams
  let token = Taro.getStorageSync('token') || ''//拿到本地缓存中存的token
  requestParams.header = {
    ...requestParams.header,
    Authorization: 'Bearer ' + token //将token添加到头部
  }
  return chain.proceed(requestParams).then(res => { return res })
}

Taro.addInterceptor(interceptor)

const request = async (method, url, params) => {
  //由于post请求时习惯性query参数使用params，body参数使用data，而taro只有data参数，使用contentType作为区分，因此此处需要做一个判断
  let contentType = 'application/json';
  if (params) contentType = params?.headers?.contentType || contentType;
  // let token = Taro.getStorageSync('token')|| '';

  const option = {
    method,
    isShowLoading: false,
    url: apiConfig.baseUrl + url,
    data: url === '/api/admin/dict/get-list' ? params.params :{
      ...params && (params?.data || params?.params),
    },
    header: {
      'content-type': contentType,
    },
    success(res) {
      // if(res?.data === null) {
      //   Taro.showToast({
      //     title: '网络繁忙、请稍后重试！',
      //     icon: 'none',
      //     duration: 2000
      //   })
      //   return
      // }
      //根据不同返回状态值3进行操作
      switch (res?.data?.code) {
        case 0:
          // Taro.showToast({
          //   title: res.data.data,
          //   icon: 'none',
          //   duration: 2000
          // })
          break;
        case 401:
          // 用户未登录
          Taro.setStorageSync('loginStatus',false);
          Taro.clearStorageSync();
          break;
        default:
          break;
      }
      return res?.data;
    },
    error(e) {
      console.log('api', '请求接口出现问题', e);
    }
  };
  const resp = await Taro.request(option);
  if(resp?.data) {
    return resp.data;
  }else {
    return {code: 0}
  }
};

export default {
  get: (url, config) => {
    return request('GET', url, config);
  },
  post: (url, config) => {
    return request('POST', url, config);
  },
  put: (url, config) => {
    return request('PUT', url, config);
  },
  delete: (url, config) => {
    return request('DELETE', url, config);
  },
  patch: (url, config) => {
    return request('PATCH', url, config);
  },
}
