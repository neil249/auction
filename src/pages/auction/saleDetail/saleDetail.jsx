import {View, Text, Image, RichText, Video, Swiper, SwiperItem} from '@tarojs/components'
import Taro,{ useLoad, getCurrentInstance, useShareAppMessage, useShareTimeline  } from '@tarojs/taro'
import './saleDetail.scss'
import React, {useState, useEffect} from "react";
import BannerPart from './../../components/swiper/bannerPart';
// import AppointModel from './appointModel'
import service from "../../services";
const List = (props) =>{
  const {imgList, videoList, detail} = props;
  const [tabList, setTabList] = useState([...props.list]);
  // 导航跳转
  function detailNavTap(val) {
    const query = Taro.createSelectorQuery();
    query._webviewId = val.ref;
    query.select(`#${val.ref}`).boundingClientRect(data => {
      if (data) {
        Taro.pageScrollTo({
          scrollTop: data.top - 30, // 100是偏移量，根据需要调整
          duration: 300 // 动画时长，可以根据需要调整
        });
      }
    }).exec();
  }
  return (
    <View className='detail_part bg-e8'>
      <View className='detail_nav flex-r-h just-between bg-white'>
        {
          tabList.map((item,idx) => {
            return <View className='detail_nav_item' onClick={() => detailNavTap(item)} key={idx}>{item.title}</View>
          })
        }
        <View className='detail_nav_item' onClick={() => detailNavTap({title: '拍卖详情',content:'拍卖详情内容',ref: 'auc'})} key={8}>拍卖详情</View>
        <View className='detail_nav_item' onClick={() => detailNavTap({title: '视频·图片',content:'视频·图片内容',ref: 'info'})} key={9}>视频·图片</View>
      </View>
      <View className="detail_list">
        {tabList.map((item,idx) => {
            return (
              <View key={`${idx}_`} id={item.ref} className="detail_list_item bg-white mar-b15">
                <View className='ft-16 mar-b5 font-w600'>{item.title}</View>
                {/*<View className='ft-14' style='height: 50vh;border:1px solid #ddd'>{item.content}</View>*/}
                <View className='pad5' style='height:auto;border:1px solid #ddd'>
                  {
                    item.ref === 'detail'? <Image src={detail.detailImgUrl} /> :
                      <RichText className='ft-14' nodes={item.content} />
                  }
                </View>
              </View>
            )
        })}
        <View key='555' id='auc' className="detail_list_item bg-white mar-b15">
          <View className='ft-16 mar-b5 font-w600'>拍卖详情</View>
          <View className='pad5 detail_item' style='border:1px solid #ddd;'>
            <View className='wid_60 ft-12 pad-tb5'>拍卖时间：{detail.auctionTime}</View>
            <View className='wid_40 ft-12 pad-b5'>竞价周期：{detail.priceCycle}小时</View>
            <View className='wid_60 ft-12 pad-b5'>加价幅度：{detail.priceAdvance}元</View>
            <View className='wid_40 ft-12 pad-b5'>处置法院：{detail.courtName}</View>
            <View className='wid_60 ft-12 pad-b5'>拍卖方式：{detail.auctionModeName}</View>
            <View className='wid_40 ft-12 pad-b5'>保证金：{detail.earnestMoney}</View>
            <View className='wid_40 ft-12 pad-b5'>评估价：{detail.evaluatePrice}</View>
          </View>
        </View>
        <View key='666' id='info' className="detail_list_item bg-white mar-b15 file_part">
          <View className='ft-16 mar-b5 font-w600'>视频·图片</View>
          <View className="img_banner_part pad-b10">
            <Swiper className='banner_img mar-t10' indicatorDots autoplay interval={2000} circular >
              {imgList && imgList.map((item,idx) => (
                <SwiperItem key={item.fileId}>
                  <Image className='banner_img' src={item.linkUrl}/>
                </SwiperItem>
              ))}
            </Swiper>
          </View>
          <View className='pad5' style='position:relative;'>
            {videoList.map((item, idx) =>{
              return (
                <View className='video_part' style='' key={item.expId}>
                  <Video style="'controls-position': 'absolute'" id={`video-${idx}`} src={item.linkUrl} className='video_detail' />
                </View>
              )
            })}
          </View>
        </View>
      </View>
    </View>
  )
};
export default function AuctionDetail(props) {
  const [detail, setDetail] = useState({})
  const { router } = getCurrentInstance();
  const list = [
    {title: '重要提示',content:'重要提示内容',ref: 'tip'},
    {title: '公告须知',content:'公告须知内容',ref: 'notice'},
    {title: '标的物详情',content:'标的物详情内容',ref: 'detail'},
    // {title: '拍卖详情',content:'拍卖详情内容',ref: 'auc'},
    // {title: '视频·图片',content:'视频·图片内容',ref: 'info'},
  ]
  const [detailList, setDetailList] = useState(list);
  const [imgList, setImgList] = useState([]);
  const [videoList, setVideoList] = useState([]);
  useEffect(() => {
    service.getAuctionsDetail({id: router.params.id}).then(res =>{
      if(res.success){
        setDetail(res.data)
      }
    })
    // 获取重要提示
    service.getDetailTip({subjcetId: router.params.id,type: 2}).then(res => {
      if(res.success){
        detailList[0].content = res.data;
        setDetailList(JSON.parse(JSON.stringify(detailList)))
      }
    })
    setTimeout(()=>{
      // 获取公告须知
      service.getDetailTip({subjcetId: router.params.id,type: 3}).then(res => {
        if(res.success){
          detailList[1].content = res.data;
          setDetailList(JSON.parse(JSON.stringify(detailList)))
        }
      })
    },600)
    setTimeout(()=>{
      // 获取视频·图片内容
      service.getDetailFile({subjcetId: router.params.id}).then(res => {
        if(res.success){
          let img = res.data.filter(item=>{
            return item.mediaType.indexOf('image') !== -1
          })
          setImgList(img)
          let video = res.data.filter(item=>{
            return item.mediaType.indexOf('video') !== -1
          })
          setVideoList(video)
        }
      })
    },1200)
    Taro.eventCenter.on('eventListener', handleScroll);
    // Taro.showShareMenu({
    //   withShareTicket: true,
    //   showShareItems: ['shareAppMessage', 'shareTimeline'],
    //   success(res) {
    //     console.log(res)
    //   },
    //   fail(e) {
    //     console.log(e)
    //   },
    // })
    // Taro.useShareAppMessage(res => {
    //   return {
    //     title: '众拍服务',
    //     path: `/pages/auction/saleDetail/saleDetail?id=${router.params.id}`
    //   }
    // })
    return () => {
      Taro.eventCenter.off('eventListener', handleScroll);
    }
  }, [])
  useShareAppMessage(res => {
    return {
      title: '众拍服务',
      path: `/pages/auction/saleDetail/saleDetail?id=${router.params.id}`
    }
  })
  useShareTimeline(() => {
    console.log('onShareTimeline')
    return {
      title: '众拍服务',
      query: `/pages/auction/saleDetail/saleDetail?id=${router.params.id}`
    }
  })
  const handleScroll = (e) => {
    console.log(e); // 这里传参
  }
  // const [modelShow, setModelShow] = useState(false)
  function callPhone(phoneNumber) {
    Taro.makePhoneCall({
      phoneNumber: phoneNumber // 电话号码
    }).then(() => {
      console.log('拨打电话成功')
    }).catch((e) => {
      console.log(`拨打电话失败:${JSON.stringify(e)}`)
    })
  }
  function appointTap() {
    if(!Taro.getStorageSync('loginStatus')){
      Taro.switchTab({url: '/pages/user/user'})
      return;
    }
    service.appointAuction({subjectId: detail.id}).then(res =>{
      if(res.success){
        Taro.showToast({title:'预约成功',});
      }else{
        if(res.msg === '未登录'){
          Taro.switchTab({url: '/pages/user/user'})
          return;
        }
        Taro.showToast({title:res.msg,icon: 'none'});
      }
    })
  }
  function getStatus(id) {
    let list = {1:'一拍', 2:'二拍', 0: '变卖'}
    return list[id]
  }

  return(
    <View className="saleDetail_part">
      <View className="head">
        {/*<BannerPart />*/}
        <Image className='header_img' mode='aspectFill' src={detail.thumbnailUrl} />
        {/*{modelShow && <AppointModel id={detail.id} closeModel={()=> setModelShow(false)} />}*/}
        <View className='head_info flex-c'>
          <Text className="ft-14">标的物编号：{detail.code}</Text>
          <View className="flex-r-h">
            <Text className="ft-14 width_40 info_item">起拍价：{detail.startPrice}元</Text>
            <Text className="ft-14 width_40 info_item">拍卖时间：{detail.auctionTime}</Text>
          </View>
          <View className="flex-r-h">
            <Text className="ft-14 width_40 info_item">标的物类型：{detail.typeName}</Text>
            <Text className="ft-14 width_40 info_item">所属法院：{detail.courtName}</Text>
          </View>
          <View className="flex-r-h just-between">
            <Text className="ft-14 info_item">拍卖平台：{detail.auctionPlatformName}</Text>
            <Text className="ft-14 info_item">预约次数：{detail.orderTotal}</Text>
            <Text className="ft-14 info_item">浏览量：{detail.viewTotal}</Text>
          </View>
          <View className="flex-r-h just-between mar-t5">
            <View className="tags">
              {getStatus(detail.auctionState) && <Text className="ft-14 mar-r10 tag">{getStatus(detail.auctionState)}</Text>}
              {detail.orderState == 1  && <Text className="ft-14 tag t-fff" style='background: #E2673C' onClick={() => appointTap()}>一键预约</Text>}
              {detail.orderState == 0  && <Text className="ft-14 tag" style='background: #DDD'>已结束</Text>}
            </View>
            <Text className="ft-14 call_btn" onClick={() => callPhone(detail.contactNumber)}>电话咨询</Text>
          </View>
        </View>
      </View>
      <List imgList={imgList} videoList={videoList} list={list} detail={detail} />
    </View>
  )
}
