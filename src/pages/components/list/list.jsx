import Taro,{ getCurrentInstance } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import './list.scss'
import React from "react";

export default function List(props) {
  const { router } = getCurrentInstance();
  if(!Taro.getStorageSync('browsing_history')){
    Taro.setStorageSync('browsing_history',[]);
  }
  const {list} = props;
  function navLink(list){
    Taro.navigateTo({url: `/pages/auction/saleDetail/saleDetail?id=${list.id}`}).then(res=>{
      let hisList = Taro.getStorageSync('browsing_history')
      let newList = hisList.filter(item => item.id !== list.id)
      newList.push(list);
      Taro.setStorageSync('browsing_history',newList);
    })
  }
  return(
    <View className='list_item mar-b5 flex-r-h bg-white' onClick={() => navLink(list)}>
      <Image mode='widthFix' className='list_item_img' src={list.thumbnailUrl?list.thumbnailUrl: require('../../../assets/noImg.png')}  />
      <View className='product flex-c mar-l15 just-between'>
        <View className="flex-c product_info">
          <Text className='font-w600 ft-14 t-333'>{list.name}</Text>
          <Text className='ft-12 t-666'>起拍价:{list.startPrice}元</Text>
          <Text className='ft-12 t-666'>拍卖时间：{list.auctionTime}</Text>
          <View className='flex-r-h just-between'>
            <Text className='ft-12 t-666'>预约次数：{list.orderTotal}人</Text>
            <Text className='ft-12 t-666'>浏览量：{list.viewTotal}人</Text>
          </View>
        </View>
        <View className='flex-r-h just-between'>
          <Text className='ft-12 t-666'/>
          <View className='ft-12 bg-theme t-fff appoint_btn'>看样预约</View>
        </View>
      </View>
      {list.orderState == 1 && <View className="tag ft-10 t-fff bg-orange">{'看样可预约中'}</View>}
    </View>
  )
}
