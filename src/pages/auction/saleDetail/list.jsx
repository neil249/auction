import Taro, { useReady, useDidShow, useDidHide, usePullDownRefresh } from '@tarojs/taro'
import { View } from '@tarojs/components'
import React, {useState} from "react";
export default function List(props) {
  const tabList = [...props.list];
  function handleScroll () {
    const scrollTop = document.documentElement.scrollTop || document.body.scrollTop
    console.log(scrollTop)
  }
  function detailNavTap(val) {
    const query = Taro.createSelectorQuery().in(this);
    query.select(`#${val.ref}`).boundingClientRect(data => {
      console.log(`ID：${val.ref},高度：${data.top}`)
      if (data) {
        Taro.pageScrollTo({
          scrollTop: data.top, // 100是偏移量，根据需要调整
          duration: 300 // 动画时长，可以根据需要调整
        });
      }
    }).exec();
  }
  return (
    <View className='detail_part'>
      <View className='detail_nav flex-r-h just-between'>
        {
          tabList.map((item,idx) => {
            return <View className='' onClick={() => detailNavTap(item)} key={idx}>{item.title}</View>
          })
        }
      </View>
      <View className="detail_list">
        {
          tabList.map((item,idx) => {
            return (
              <View key={idx} id={item.ref} className="mar-b20">
                <View className='ft-16 mar-b10'>{item.title}</View>
                <View className='ft-14' style='height: 50vh;border:1px solid red'>{item.content}</View>
              </View>
            )
          })
        }
      </View>
    </View>
  )
}
