import { View, Image, Input, Picker, Button } from '@tarojs/components'
import Taro, { useLoad, useShareAppMessage, useShareTimeline } from '@tarojs/taro'
import React, { useState, useEffect } from "react";
import { AtTabs, AtTabsPane } from 'taro-ui'
import service from "../services/index";
import './home.scss'
import List from "../components/list/list";
import BannerPart from "../components/swiper/bannerPart";
const HeadPart = (props) => {
  const [keyVal, setKeyVal] = useState('');
  const [loginStatus, setLoginStatus] = useState(false);
  useEffect(()=>{
    if(Taro.getStorageSync('token') && Taro.getStorageSync('loginStatus')){
      setLoginStatus(true);
    }
  },[]);
  useShareAppMessage(res => {
    return {
      title: '众拍服务',
      path: `/pages/home/home`
    }
  })
  useShareTimeline(() => {
    console.log('onShareTimeline')
    return {
      title: '众拍服务',
      query: `/pages/home/home`,
    }
  })
  function keyValChange(e){
    let val = e.target.value
    setKeyVal(val)
    if(val === ''){
      props.getList('', false);
      return
    }
    props.getList(val, !!val)
  }
  function clearTap(){
    setKeyVal('')
    props.getList('', false)
  }
  function searchTap(){
    props.getList(keyVal, !!keyVal)
  }
  return(
    <View className='head'>
      <View className='flex-r-h just-between'>
        <View className="logo mar-r10">
          <Image className='logo_img' src={require('../../assets/logo.png')} />
        </View>
        <View className="search flex-r-h">
          <View className='iconfont icon-search t-gray' />
          <Input value={keyVal} onInput={keyValChange} className='search_ipt mar-l10 ft-14' placeholder='请输入要查询的标的物' />
          {keyVal && <View onClick={()=>clearTap()} className='iconfont icon-clear t-gray' />}
        </View>
        {!loginStatus && !keyVal && <View className="login_btn ft-16 mar-l10" onClick={()=> Taro.switchTab({url: '/pages/user/user'})}>登录</View>}
        {/*{keyVal && <View className="login_btn ft-16 mar-l10" onClick={()=> searchTap(keyVal)}>搜索</View>}*/}
      </View>
    </View>
  )
};
const SearchPart = (props) =>{
  return(
    <View className="search_part">
      {
        props.lists.map(item => <List list={item} key={item.id} />)
      }
    </View>
  )
};
const Tabs = (props) => {
  const [roomList, setRoomList] = useState([]);
  const [carList, setCarList] = useState([]);
  const [otherList, setOtherList] = useState([]);
  const [typeList, setTypeList] = useState([]);
  useEffect(() =>{
    getList(0);
    getType()
  },[]);
  const tabList = [{ title: '房产' }, { title: '机动车' }, { title: '资产' },{title: '更多'}];
  const fastNav = [{name:'最新',value:'AuctionTime'},{name:'最热',value:'ViewTotal'}];
  const [current, setCurrent] = useState(0);
  const [fastNavIdx, setFastNavIdx] = useState('AuctionTime');
  function handleClick(e){
    if(e===3) {
      Taro.setStorageSync('moreType', 'more')
      setTimeout(() =>{
        Taro.switchTab({url: '/pages/auction/auction?type=more'})
      }, 50)
      return
    }
    setCurrent(e)
    getList(e,fastNavIdx)
  }
  function fastNavTap(val) {
    setFastNavIdx(val)
    getList(current,val)
  }
  const getType = () =>{
    service.getAuctionType(["subjectType"]).then(res => {
      if(res.success){
        setTypeList(res.data.subjectType)
      }
    })
  };
  function getList(type = 0, orderBy = 'AuctionTime'){
    let data = {currentPage:1, pageSize: 20, filter: {key: null, type, orderBy,}};
    service.getAuctions(data).then(res => {
      if(res.success === true && res.data) {
        console.log(res.data)
        if(type === 0) setRoomList(res.data.list)
        if(type === 1) setCarList(res.data.list)
        if(type === 2) setOtherList(res.data.list)
      }
    })
  }
  return(
    <View className='tab_part'>
      <View className='flex-r-h tab_type pad-t10'>
        {fastNav.map((item, idx) => <View key={idx} onClick={()=> fastNavTap(item.value)} className={`tab_type_item ft-12 ${fastNavIdx === item.value ? 'active':''}`}>{item.name}</View>)}
      </View>
      <AtTabs current={current} tabList={tabList} onClick={handleClick}>
        <View style='height:25px' />
        <AtTabsPane className='sale_body mar-t10' current={current} index={0} >
          {roomList && roomList.map((item,idx) => <List list={item} key={item.id} />)}
        </AtTabsPane>
        <AtTabsPane className='sale_body mar-t10' current={current} index={1}>
          {carList && carList.map((item,idx) => <List list={item} key={item.id} />)}
        </AtTabsPane>
        <AtTabsPane className='sale_body mar-t10' current={current} index={2}>
          {otherList && otherList.map((item,idx) => <List list={item} key={item.id} />)}
        </AtTabsPane>
      </AtTabs>
    </View>
  )
};
export default function Home() {
  const [bannerList, setBannerList] = useState([]);
  const [lists, setLists] = useState([]);
  const [searchModel, setSearchModel] = useState(false);
  useEffect(() => {
    getBannerList();
    // Taro.showShareMenu({
    //   withShareTicket: true,
    //   showShareItems: ['shareAppMessage', 'shareTimeline'],
    // })
    // Taro.useShareAppMessage(res => {
    //   return {
    //     title: '众拍服务',
    //     path: 'pages/home/home',
    //   }
    // })
  }, []);
  function getBannerList(){
    service.getBanner({filter:{orderBy: 'ViewTotal'},pageSize: 5,}).then(res =>{
      if(res.success){
        setBannerList(res.data.list)
      }
    });
  }
  function getLists(val, model) {
    setSearchModel(model)
    if(model){
      let data = {currentPage:1, pageSize: 20, filter: {key: val,}};
      service.getAuctions(data).then(res => {
        if(res.success === true && res.data) {
          console.log(res.data)
          setLists(res.data.list)
        }
      })
    }else {
      setLists([])
    }
  }
  return (
    <View className='home_part'>
      <View className="home_head_part">
        <HeadPart getList={getLists} />
        {searchModel && <SearchPart lists={lists} />}
        {bannerList.length ? <BannerPart list={bannerList} /> : ''}
        <Image mode='widthFix' style='width:94vw' className='mar-t10' src={require('../../assets/flow.png')} alt='拍卖流程' />
      </View>
      <Tabs />
    </View>
  )
}
