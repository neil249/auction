import { View, Image, Input, Text, Button, Swiper, SwiperItem } from '@tarojs/components'
import Taro, { useLoad } from '@tarojs/taro'
import React, { useState } from "react";
import './bannerPart.scss'
export default function BannerPart(props){
  const {list} = props;
  function goDetail(list){
    Taro.navigateTo({url: `/pages/auction/saleDetail/saleDetail?id=${list.id}`}).then(res=>{
      let hisList = Taro.getStorageSync('browsing_history')
      let newList = hisList.filter(item => item.id !== list.id)
      newList.push(list);
      Taro.setStorageSync('browsing_history',newList);
    })
  }
  return (
    <View className="banner_part">
      <Swiper className='banner_img mar-t10' indicatorDots autoplay interval={2000} circular >
        {list && list.map((item,idx) => (
          <SwiperItem key={idx}>
            {/*<View className="banner_img" style='background: #dfdfdf'>这是一张图片</View>*/}
            <Image onClick={() => goDetail(item)} className='banner_img' src={item.thumbnailUrl}/>
          </SwiperItem>
        ))}
      </Swiper>
    </View>
  )
}
