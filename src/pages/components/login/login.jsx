import Taro,{ useLoad } from '@tarojs/taro'
import React, { useState } from "react";
import { View, Text, Image, Input, Button } from '@tarojs/components'
import service from "../../services/index";
import './login.scss'
export default function Login(props){
  const [phone, setPhone] = useState('')
  const [phoneCode, setPhoneCode] = useState('')
  const [btnText, setBtnText] = useState('获取验证码')
  const [codeId, setCodeId] = useState('95dac0c6-7014-471f-95fe-34a714d226ff')
  function phoneChange(e){setPhone(e.target.value)}
  function codeChange(e){setPhoneCode(e.target.value)}
  function isValidChinesePhoneNumber(phoneNumber) {
    const phoneRegex = /^1\d{10}$/;
    return phoneRegex.test(phoneNumber);
  }
  // 获取验证码
  function getCode() {
    if(!phone){
      Taro.showToast({icon: 'none', title: '请输入手机号'});
      return
    }
    if(!isValidChinesePhoneNumber(phone)){
      Taro.showToast({icon: 'none', title: '请输入正确的手机号'});
      return
    }
    if(btnText === '获取验证码'){
      let countdown = 60;
      setBtnText(countdown);
      service.getCode({mobile: phone}).then(res => {
        if(res.success){
          setCodeId(res.data)
          Taro.showToast({icon: 'none', title: '发送成功！'});
          let timer = setInterval(()=>{
            if(countdown > 0) {
              countdown--;
              setBtnText(countdown);
            } else {
              clearInterval(timer)
              setBtnText('获取验证码')
            }
          },1000)
        }
      })
    }
  }
  // 获取用户信息
  function getUserInfo() {
    service.getUserInfo().then(res =>{
      if(res.success){
        Taro.setStorageSync('userInfo',res.data);
        props.upDate(res.data)
      }
    })
  }
  // 登录
  function loginTap (e){
    if(!phone || !phoneCode){
      Taro.showToast({icon: 'none',title: '请输入手机号或验证码'});
      return
    }
    service.login({mobile: phone,code: phoneCode,codeId:codeId}).then(res => {
      if(res.success){
        Taro.showToast({title:'登录成功！',});
        Taro.setStorageSync('loginStatus',true);
        Taro.setStorageSync('token',res.data.token);
        getUserInfo()
        setTimeout(()=>{
          e.onChangeTap(true);
        },500)
      }
    })
  }
  return(
    <View className='login_'>
      <View className="logo_part flex-r-h just-center">
        <Image className='logo_img' src={require('../../../assets/logo.png')} />
        <Text className='ft-32 font-w600 mar-l20'>众拍拍卖</Text>
      </View>
      <View className="login_part">
        {/*<Input placeholder='请输入手机号' />*/}
        <View className='account_type'>
          <Input value={phone} onInput={phoneChange} className='user_account' type='text' placeholder='请输入手机号' />
          <View className='code_part'>
            <Input value={phoneCode} onInput={codeChange} className='code' type="number" placeholder='请输入验证码' />
            <View onClick={() => {getCode()}} className='getCode_btn bg-theme'>{btnText}</View>
          </View>
          <View className='ft-14 pad-b5 pad-l5 t-gray sign_text'> <Text className='t-theme font-w600'>未注册</Text>过的手机号将自动创建新账号并登录</View>
          <Button onClick={() => loginTap(props)} className="primary-btn mar-b10">登录</Button>
        </View>
      </View>
    </View>
  )
}
