
import { useLaunch } from '@tarojs/taro'
import './app.scss'

function App({ children }) {
  useLaunch(() => {
    if(!Taro.getStorageSync('loginStatus')){
      Taro.setStorageSync('loginStatus', false)
    }
  });
  return children
}

export default App
