import httpService from './httpService';
import Taro from '@tarojs/taro';
// 登录接口
function login(params) {
  return httpService.post('/api/admin/auth/mobile-login', {params})
}
// 获取用户信息
function getUserInfo() {
  return httpService.get('/api/admin/auth/get-user-profile')
}
// 获取登录验证码
function getCode(params) {
  params= {...params};
  return httpService.post(`/api/admin/auth/send-sms-code`, {params})
}
// 获取轮播图
function getBanner(params) {
  return httpService.post(`/api/auction/subject-matter/get-page`, {params})
}
//获取咨询
function getNews(params){
  return httpService.post(`/api/auction/new/get-page`, {params})
}
//获取标的物列表
function getAuctions(params){
  return httpService.post(`/api/auction/subject-matter/get-page`, {params})
}
//获取标的物详情
function getAuctionsDetail(params){
  return httpService.get(`/api/auction/subject-matter/get`, {params})
}
// 获取标的物类型
function getAuctionType(params) {
  return httpService.post(`/api/admin/dict/get-list`, {params})
}
// 用户预约
function appointAuction(params){
  return httpService.post(`/api/auction/appointment/record`, {params})
}
// 获取区域
function getRegion() {
  return httpService.get(`/api/admin/region/get-all-region`, {})
}
// 获取法院 /
function getCourList(params){
  return httpService.post(`/api/auction/court/get-page`, {params})
}
// 预约记录
function getAppoint(params){
  return httpService.post(`/api/auction/appointment/get-page`, {params})
}
// 浏览记录
function getViewList(params){
  return httpService.post(`/api/auction/view-log/get-page`, {params})
}
// 标的物统计
function getCount() {
  return httpService.get(`/api/auction/workbench/get-subject-info`, {})
}
// 重要提示
function getDetailTip(params) {
  return httpService.get(`/api/auction/subject-matter-expand/get-subject-describe`, {params})
}
// 详情文件
function getDetailFile(params) {
  return httpService.get(`/api/auction/subject-matter-expand/get-subject-file`, {params})
}
export default {
  getAppoint,
  getViewList,
  getCourList,
  appointAuction,
  getAuctionType,
  getAuctions,
  getAuctionsDetail,
  getDetailTip,
  getDetailFile,
  getRegion,
  getNews,
  getBanner,
  getCode,
  getUserInfo,
  getCount,
  login
}
