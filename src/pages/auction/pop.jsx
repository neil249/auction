import {View, Text, Image, Input} from '@tarojs/components'
import Taro, {useDidShow} from '@tarojs/taro'
import React, {Fragment, useState,useEffect} from "react";
import service from "../services";
import './auction.scss'

export default function Pop(props) {
  const [popShow, setPopShow] = useState('');
  useDidShow(()=>{
    setTimeout(()=>{
      if(Taro.getStorageSync('moreType') === 'more'){
        setPopShow('saleType')
      }
      Taro.removeStorageSync('moreType')
    }, 200)
  });
  useEffect(()=>{
    getRegion()
    getType()
  },[provinceList, cityList, courtList])
  // 区域信息处理----开始
  const [provinceList, setProvinceList] = useState([]);
  const [provinceIdx, setProvinceIdx] = useState(null);
  const [cityList, setCityList] = useState([]);
  const [cityIdx, setCityIdx] = useState([]);
  //获取区域信息
  const getRegion =() => {
    if(Taro.getStorageSync('region')){
      console.log(`缓存城市信息输出····`);
      let region = JSON.parse(Taro.getStorageSync('region'));
      setProvinceList(region);
      // setProvinceIdx(region[0]?.value)
      setCityList([{label:'不限', value: null},...region[0]?.children]);
      getCourt();
      return;
    }
    service.getRegion().then((res) =>{
      if(res.success){
        console.log(`接口城市信息输出····`);
        Taro.setStorageSync('region', JSON.stringify(res.data));
        setProvinceList(res.data);
        setCityList([{label:'不限', value: null},...res.data[0]?.children]);
        getCourt();
      }
    })
  };
  // 点击省份
  function provinceTap(item, idx) {
    setProvinceIdx(item.value);
    setPopText(item.label);
    setCityList([{label:'不限', value: null},...item.children]);
    setCityIdx([])
  }
  // 点击市区
  function cityTap(data, idx) {
    if(data.value===null){
      setCityIdx([null])
    }else{
      let cityCheckedList = cityIdx.filter(item => item !== null);
      if(cityCheckedList.indexOf(data.value) === -1){
        cityCheckedList.push(data.value)
      }else {
        cityCheckedList = cityIdx.filter(item => item !== data.value);
      }
      setCityIdx(Array.from(new Set(cityCheckedList)))
    }
    setPopText(`${popText}-${data.label}`);
  }
  // 区域信息处理----结束
  // 标的物类型处理----开始
  const [typeList, setTypeList] = useState([]);
  const [typeIdx, setTypeIdx] = useState([]);
  const getType = () =>{
    service.getAuctionType(["subjectType"]).then(res => {
      if(res.success){
        setTypeList(res.data.subjectType)
      }
    })
  };
  function typeTap(data) {
    let typeCheckedList = [...typeIdx];
    if(typeCheckedList.indexOf(data.value) === -1){
      typeCheckedList.push(data.value)
    }else {
      typeCheckedList = typeCheckedList.filter(item => item!== data.value)
    }

    setTypeIdx(Array.from(new Set(typeCheckedList)));
    setPopText(data.name)
  }
  // 标的物类型处理----结束
  // 法院处理 ----开始
  const [courtList, setCourtList] = useState([]);
  const [courIdx, setCourIdx] = useState([])
  // 获取法院信息
  const getCourt = (type = 'search') => {
    let data = {
      currentPage:1,
      pageSize:50,
      filter: {
        provinceCode: type === 'search' ? provinceIdx : null,
        cityCode:  type === 'search' ? cityIdx.join(',') : null
      }
    };
    // 获取法院信息
    service.getCourList(data).then(res =>{
      if(res.success){
        setCourtList(res.data.list)
        setCourtText('法院')
      }
    })
  }
  // 点击法院
  function courtTap(data){
    let courtCheckedList = [...courIdx];
    if(courtCheckedList.indexOf(data.id) === -1) {
      courtCheckedList.push(data.id)
    }else {
      courtCheckedList = courtCheckedList.filter(item => item !== data.id)
    }
    setCourIdx(Array.from(new Set(courtCheckedList)))
    setPopText(data.name)
  }
  // 法院处理 ----结束
  // 价格处理 ----开始
  const price = [{name:'不限',value: null},
    {name:'0-10万',value: [0,100000]},
    {name:'10万-50万',value: [100000,500000]},
    {name:'50万-100万',value: [500000,1000000]},
    {name:'100万-200万',value: [1000000,2000000]},
    {name:'200万-500万',value: [2000000,5000000]},
    {name:'500万-1000万',value: [5000000,10000000]},
    {name:'1000万以上',value: [10000000,null]},
  ];
  const [startPrice, setStartPrice] = useState(null);
  const [endPrice, setEndPrice] = useState(null);
  const [priceList, setPriceList] = useState(price);
  const [priceIdx, setPriceIdx] = useState(null);
  function priceTap(data) {
    setStartPrice(null)
    setEndPrice(null)
    setPriceIdx(data.value)
    setPopText(data.name)
  }
  // 价格处理 ----结束
  const mores = [
    {
      name: '拍卖状态',
      type: 'state',
      list:['不限', '一拍', '二拍', '变卖', '成交', '以物抵债'],
      selected: '不限',
    },
    {
      name:'拍卖时间',
      type: 'date',
      list:['不限', '今天', '3天内', '7天内'],
      selected: '不限',
    }
  ];
  const [moreList, setMoreList] = useState(mores);
  const [moreIdx, setMoreIdx] = useState('state');
  const [moreItemList, setMoreItemList] = useState(moreList[0].list)
  const [moreItemIdx, setMoreItemIdx] = useState(0)

  function moreTap(data,idx){
    setMoreItemIdx(data.selected)
    setMoreIdx(data.type)
    setMoreItemList(data.list)
  }
  function moreItemTap(val, idx, type){
    setMoreItemIdx(val);
    let changeList = moreList.map(item =>{
      if(item.type === 'state' && type === 'state'){
        if(val === '不限'){
          item.selected = val
        }else {
          item.selected = item.selected.replace('不限,', '');
          item.selected = item.selected.replace('不限', '');
          item.selected.indexOf(val) === -1 ?
            item.selected = item.selected + `,${val}` : item.selected = item.selected.replaceAll(`,${val}`, '')
        }
      } else {
        if(item.type === moreIdx) item.selected = val
      }
      return item;
    });
    setMoreList(changeList);
    let selectedList = moreList.filter(item=>{
      return item.selected !== '不限'
    });
    setPopText(selectedList.length!== 0?`+${selectedList.length}` : '更多')
  }
  function screenNavTap(val) {
    if(val === popShow){
      setPopShow('');
      return
    }
    setPopShow(val)
  }
  function popTap(e){
    if(e.target.idx === '1' || e.target.dataset.idx === '1')setPopShow('');
  }
  function getToday() {
    const today = new Date();
    return today.toISOString().split('T')[0];
  }

// 获取3天内的日期
  function getLast3Days() {
    const today = new Date();
    const threeDaysAgo = new Date(today);
    threeDaysAgo.setDate(threeDaysAgo.getDate() - 3);
    return threeDaysAgo.toISOString().split('T')[0];
  }

// 获取7天内的日期
  function getLast7Days() {
    const today = new Date();
    const sevenDaysAgo = new Date(today);
    sevenDaysAgo.setDate(sevenDaysAgo.getDate() - 7);
    return sevenDaysAgo.toISOString().split('T')[0];
  }
  function getDate(date){
    let dateList = {'不限': null, '今天':[getToday(),getToday()], '3天内':[getToday(),getLast3Days()], '7天内':[getToday(),getLast7Days()]}
    return dateList[date]
  }
  function subTap() {
    setTimeout(()=>{
      if(startPrice && endPrice && Number(startPrice) > Number(endPrice)){
        Taro.showToast({title:'价格输入有误',icon:'none'})
        return
      }
      let auctionState = '';
      let date = null;
      let auctionStateList = {'不限': null,'一拍': 1,'二拍':2,'变卖': 0, '成交':3,'以物抵债': 4};

      moreList.forEach(item=>{
        if(item.type === 'state'){
          let stateList = item.selected.split(',');
          stateList.forEach(item =>{
            if(item) auctionState += `${auctionStateList[item]},`
          });
          if(auctionState === 'null' ||auctionState === 'null,') auctionState = null
        }
        if(item.type === 'date'){
          date = getDate(item.selected);
        }
      })
      let filter = {
        beginTime: date? date[0]: null,
        endTime: date? date[1]: null,
        provinceCode: provinceIdx,
        cityCode: cityIdx.join(','),
        type: typeIdx.join(','),
        auctionState: auctionState,
        orderState:  null,
        orderBy: null,
        priceRange: startPrice && endPrice ? [startPrice+'0000' , endPrice + '0000'] :priceIdx,
        courtId: courIdx.join(',')
      }
      switch (popShow) {
        case 'region':
          setRegionText(popText);
          setCourtText('法院')
          setCourIdx([])
          getCourt();
          break;
        case 'court':
          setCourtText(popText);
          break;
        case 'price':
          setPriceText(startPrice && endPrice ? `${startPrice}万-${endPrice}万` :popText || '价格');
          break;
        case 'saleType':
          setTypeText(popText);
          break;
        case 'more':
          setMoreText(popText)
          break;
        default:
          console.log(popShow)
      }
      props.subTap(filter);
      setPopShow('');
    },100)
  }
  function resetTap() {
    let filter = {};
    setPopText('');
    setRegionText('区域');
    setCourtText('法院');
    setTypeText('类型');
    setPriceText('价格');
    setMoreText('更多');
    setProvinceIdx(null);
    setCityIdx([]);
    setCourIdx([]);
    setTypeIdx([]);
    setMoreList(mores);
    setPriceIdx(null);
    setMoreIdx('state')
    setMoreItemIdx(0)
    setMoreItemList(moreList[0].list)
    getCourt('reset')
    props.subTap(filter);
    setPopShow('');
  }
  const [popText, setPopText] = useState('');
  const [regionText, setRegionText] = useState('区域');
  const [courtText, setCourtText] = useState('法院');
  const [typeText, setTypeText] = useState('类型');
  const [priceText, setPriceText] = useState('价格');
  const [moreText, setMoreText] = useState('更多');
  const startPriceChange = (e) =>{
    setPriceIdx(null)
    setStartPrice(e.detail.value)
  }
  const endPriceChange = (e) =>{
    setPriceIdx(null)
    setEndPrice(e.detail.value)
  }

  return(
    <View className="screen">
      <View className="screen_nav flex-r-h just-between mar-t15">
        <View className="region flex-r-h text_one" onClick={() => screenNavTap('region')}>
          <Text className={`${regionText !=='区域' || popShow === 'region' ? 't-theme': ''} ft-14 pad-r5`}>{regionText}</Text>
          <Image src={require(`../../assets/${popShow === 'region'?'top':'down'}.png`)} className='down' />
        </View>
        <View className="region flex-r-h text_one" onClick={() => screenNavTap('court')}>
          <Text className={`${courtText !=='法院' || popShow === 'court' ? 't-theme': ''} ft-14 pad-r5`}>{courtText}</Text>
          <Image src={require(`../../assets/${popShow === 'court'?'top':'down'}.png`)} className='down' />
        </View>
        <View className="region flex-r-h text_one" onClick={() => screenNavTap('saleType')}>
          <Text className={`${typeText !=='类型' || popShow === 'saleType' ? 't-theme': ''} ft-14 pad-r5`}>{typeText}</Text>
          <Image src={require(`../../assets/${popShow === 'saleType'?'top':'down'}.png`)} className='down' />
        </View>
        <View className="region flex-r-h text_one" onClick={() => screenNavTap('price')}>
          <Text className={`${priceText !=='价格' || popShow === 'price' ? 't-theme': ''} ft-14 pad-r5`}>{priceText}</Text>
          <Image src={require(`../../assets/${popShow === 'price'?'top':'down'}.png`)} className='down' />
        </View>
        <View className="region flex-r-h text_one" onClick={() => screenNavTap('more')}>
          <Text className={`${moreText !=='更多' || popShow === 'more' ? 't-theme': ''} ft-14 pad-r5`}>{moreText}</Text>
          <Image src={require(`../../assets/${popShow === 'more'?'top':'down'}.png`)} className='down' />
        </View>
      </View>
      {popShow && <View onClick={popTap} className="pop" idx='1' data-idx='1'>
        {/*省市选择权*/}
        {popShow === 'region' && <View className="region_part flex-r" idx='2' data-idx='2'>
          <View className="province flex-c pad-t15">
            {provinceList.map((item, idx) => {
              return <View onClick={()=> provinceTap(item, idx) } key={item.label}  className={`${item.value === provinceIdx ? 'province_active':''} province_item ft-16`}>{item.label}</View>
            })}
          </View>
          <View className="city flex-c pad-t15">
            {cityList.map((item, idx) => {
              return <View onClick={()=> cityTap(item, idx) } key={item.value} className={`${cityIdx.indexOf(item.value) !== -1  ? 'city_active' : ''} city_item ft-14`}>{item.label}</View>
            })}
          </View>
        </View>}
        {/*法院选择*/}
        {popShow === 'court' && <View className="court_part flex-wrap just-between">
          {courtList.map((item, idx) => {
            return <View key={idx} onClick={() => courtTap(item)} className={`${courIdx.indexOf(item.id) !== -1 ? 'court_active':''} court_item ft-14 mar-t10`}>{item.name}</View>
          })}
        </View>}
        {/*价格选择*/}
        {popShow === 'price' && <View className="court_part flex-wrap just-between">
          {priceList.map((item, idx) => {
            return <View key={idx} onClick={() => priceTap(item)} className={`${item.value === priceIdx? 'court_active':''} court_item ft-14 mar-t10`}>{item.name}</View>
          })}
          <View className='flex-r-h just-around price_ipt mar-t10'>
            <View className='flex-r-h' style='width:28vw'>
              <Input value={startPrice} onInput={startPriceChange} className='code' type="number" placeholder='请输入' />
              <Text className='ft-14 t-gray'>万</Text>
            </View>
            <Text>————</Text>
            <View className='flex-r-h' style='width:28vw'>
              <Input value={endPrice} onInput={endPriceChange} className='code' type="number" placeholder='请输入' />
              <Text className='ft-14 t-gray'>万</Text>
            </View>
          </View>
        </View>}
        {/*类型选择*/}
        {popShow === 'saleType' && <View className="court_part flex-wrap just-between">
          {typeList.map((item, idx) => {
            return <View key={item.id} onClick={() => typeTap(item)} className={`${typeIdx.indexOf(item.value) !== -1? 'court_active':''} court_item ft-14 mar-t10`}>{item.name}</View>
          })}
        </View>}
        {/*更对*/}
        {popShow === 'more' && <View className="more_part flex-r">
          <View className="left_part flex-c pad-t15">
            {moreList.map((item, idx) => {
              return <View onClick={()=> moreTap(item,idx) } key={item.name}  className={`${item.type === moreIdx ? 'province_active':''} province_item ft-16`}>{item.name}</View>
            })}
          </View>
          <View className="right_part flex-c pad-t15">
            {moreIdx === 'state' && moreItemList.map((item, idx) => {
              return <View onClick={()=> moreItemTap(item, idx, 'state') } key={idx} className={`${moreList[0].selected.indexOf(item) !== -1 ? 'city_active' : ''} city_item ft-14`}>{item}</View>
            })}
            {moreIdx === 'date' && moreItemList.map((item, idx) => {
              return <View onClick={()=> moreItemTap(item, idx, 'date') } key={idx} className={`${moreItemIdx === item ? 'city_active' : ''} city_item ft-14`}>{item}</View>
            })}
          </View>
        </View>}
        <View className="btn_part flex-r-h just-between">
          <View onClick={()=> resetTap()} className="reset btn bg-e8">重置</View>
          <View onClick={()=> subTap()} className="submit btn bg-theme t-fff">确定</View>
        </View>
      </View>}
    </View>
  )
  // return(
  //   <Fragment>
  //     {/*{region && region.length && <ScreenNav region={region} />}*/}
  //
  //   </Fragment>
  //
  // )
}
