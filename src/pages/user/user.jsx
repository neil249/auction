import { View, Text, Image } from '@tarojs/components'
import Taro,{ useLoad } from '@tarojs/taro'
import './user.scss'
import React, {useEffect, useState} from "react";
import Login from '../components/login/login'
import service from './../services/index'
export default function User() {
  const [loginStatus, setLoginStatus] = useState(false);
  const [userInfo, setUserInfo] = useState({})
  const [countDetail, setCountDetail] = useState(null)
  useEffect(()=>{
    if(Taro.getStorageSync('token') && Taro.getStorageSync('loginStatus')){
      setLoginStatus(true);
      afterLogin()
    }
  }, [])

  function logOut(){
    Taro.showModal({
      title: '',
      content: '是否退出登录？',
      confirmText: '退出',
      confirmColor: '#D9102B',
      success: function (res) {
        if (res.confirm) {
          setLoginStatus(false)
          Taro.setStorageSync('loginStatus',false);
          Taro.clearStorageSync();
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  };
  function goRecordList(id) {
    Taro.navigateTo({url: `/pages/user/appoint/appointList/appointList?type=${id}`})
  }
  function afterLogin() {
    setUserInfo(Taro.getStorageSync('userInfo'));
    getCount()
  }
  function getCount() {
    service.getCount().then(res =>{
      if(res.success){
        setCountDetail(res.data)
      }
    })
  }
  function goList() {
    Taro.switchTab({url: '/pages/auction/auction'})
  }
  return (
    <View className='user_part'>
      {!loginStatus && <Login upDate={()=>afterLogin()} onChangeTap={setLoginStatus} />}
      <View className='head flex-r-h'>
        <Image src={require('../../assets/user.png')} className='user_img' />
        <View className='flex-r-h flex-1 just-between' onClick={() => logOut()}>
          <View className='ft-16'>{userInfo.name}</View>
          <View className='iconfont icon-right ft-20 font-w600 mar-r20'/>
        </View>
      </View>
      {countDetail && <View className='statistics box_part'>
        <View className=' flex-r-h just-around'>
          <View className='flex-c-h' onClick={() => goList()}>
            <View className='flex-r-h'>
              <View className="iconfont icon-browse color_1 mar-r5 ft-24"/>
              <Text className='t-gray ft-14'>案件量</Text>
            </View>
            <Text className='ft-18 font-w600'>{countDetail?.total}</Text>
          </View>
          <View className='flex-c-h'>
            <View className='flex-r-h'>
              <View className="iconfont icon-arrange color_2 mar-r5 ft-24"/>
              <Text className='t-gray ft-14'>浏览量</Text>
            </View>
            <Text className='ft-18 font-w600'>{countDetail?.viewTotal}</Text>
          </View>
          <View className='flex-c-h'>
            <View className='flex-r-h'>
              <View className="iconfont icon-deal color_3 mar-r5 ft-24"/>
              <Text className='t-gray ft-14'>预约数</Text>
            </View>
            <Text className='ft-18 font-w600'>{countDetail?.appointmentTotal}</Text>
          </View>
        </View>
        <View className='mar-t5 flex-r-h just-around'>
          <View className='flex-c-h'>
            <View className='flex-r-h'>
              <Image style="width:24px;height:24px" src={require('../../assets/caseAmount.png')} />
              <Text className='t-gray ft-14 pad-l5'>案件额</Text>
            </View>
            <Text className='ft-18 font-w600'>{countDetail?.caseAmount || 0}</Text>
          </View>
          <View className='flex-c-h'>
            <View className='flex-r-h'>
              <Image style="width:24px;height:24px" src={require('../../assets/turnover.png')} />
              <Text className='t-gray ft-14 pad-l5'>成交额</Text>
            </View>
            <Text className='ft-18 font-w600'>{countDetail?.turnover || 0}</Text>
          </View>
          <View className='flex-c-h' style='width:70px'/>
        </View>
      </View>}
      {!countDetail && <View className="box_part flex-r-h nav">
        <View className='flex-c-h' onClick={()=> goRecordList('1')}>
          <Image src={require('../../assets/list.png')} />
          <Text className='ft-14 font-w600'>我的预约</Text>
        </View>
        <View className='flex-c-h' onClick={()=> goRecordList('2')}>
          <Image src={require('../../assets/mark.png')} />
          <Text className='ft-14 font-w600'>浏览记录</Text>
        </View>
      </View>}
    </View>
  )
}
