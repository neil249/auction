export default defineAppConfig({
  pages: [
    'pages/home/home',
    'pages/auction/auction',
    'pages/auction/saleDetail/saleDetail',
    'pages/guidance/guidance',
    'pages/components/newsDetail/newsDetail',
    'pages/user/appoint/appointList/appointList',
    'pages/user/user'
  ],
  tabBar: {
    color: '#7A7E83',
    selectedColor: '#D9102B',
    backgroundColor: '#FFFFFF',
    list:[
      {pagePath: 'pages/home/home', text: '首页',
        iconPath: 'assets/tabBar/home.png',
        selectedIconPath: 'assets/tabBar/selectHome.png'
      },
      {pagePath: 'pages/auction/auction', text: '预约',
        iconPath: 'assets/tabBar/auction.png',
        selectedIconPath: 'assets/tabBar/selectAuction.png'},
      {pagePath: 'pages/guidance/guidance', text: '资讯',
        iconPath: 'assets/tabBar/guidance.png',
        selectedIconPath: 'assets/tabBar/selectGuidance.png'},
      {pagePath: 'pages/user/user', text: '我的',
        iconPath: 'assets/tabBar/user.png',
        selectedIconPath: 'assets/tabBar/selectUser.png'
      },
    ]
  },
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  }
})
